from setuptools import setup

setup (
    name='pybootstrap',
    version='0.0.1',
    description='A bootstrap package for new python projects',
    url='http://github.com/storborg/funniest',
    author='Pierre Chanquion',
    author_email='pierre.chanquion@gmail.com',
    license='MIT',
    packages=['pybootstrap'],
    zip_safe=False
)
